﻿using System;
namespace CSharpCraft
{
	public class CreateGame
	{
		//function that starts new game
		public void startGame()
		{
			//Initialize classes from GameProperties.cs
			GameProperties newGame = new GameProperties();
			Player newPlayer = new Player();

			newGame.setTerrain();
			newGame.buildTerrain();
			newPlayer.createPlayerHealth();
			newPlayer.askForNewName();
			newPlayer.changeName();
			newPlayer.getPlayerName();
			newPlayer.checkPlayerHealth();
		}
	}
}
