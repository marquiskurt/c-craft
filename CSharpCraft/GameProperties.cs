﻿using System;
namespace CSharpCraft
{
	public class GameProperties
	{
		int blockHeight = 256;
		string defaultTerrain = "plains";

		public void setTerrain()
		{
			Console.WriteLine("Enter the type of terrain you want.");
			defaultTerrain = Console.ReadLine();

		}

		public void buildTerrain()
		{
			Console.WriteLine("Setting block height to " + blockHeight + " blocks.");
			Console.WriteLine("Generating world type (" + defaultTerrain + ")...");
		}

		public GameProperties()
		{
		}
	}
}
