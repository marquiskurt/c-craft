﻿using System;
namespace CSharpCraft
{
	public class Player
	{
		string name = "Steve";
		string newName;
		int defaultHealth = 3;
		int currentHealth;


		public void createPlayerHealth()
		{
			currentHealth = defaultHealth;
		}

		public void askForNewName()
		{
			Console.WriteLine("Name the new player: ");
			newName = Console.ReadLine();
		}

		public void changeName()
		{
			name = newName;
		}

		public void getPlayerName()
		{
			Console.WriteLine("Player name: " + name);
		}

		public void checkPlayerHealth()
		{
			Console.WriteLine("Current health: " + currentHealth);
		}
	}
}
