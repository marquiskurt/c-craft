﻿using System;

namespace CSharpCraft
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("C# Craft! \n(C) 2017 | Marquis Kurt. Licensed under GNU GPL v.3 \nWhat do you want to do? (ver | start | quit)");
			StartProgram:
				string gameChoice = Console.ReadLine();

			switch (gameChoice)
			{
				case "ver":
					Console.WriteLine("C# Craft! \n(C) 2017 | Marquis Kurt. \nVersion Aplha 0.1 \nLicensed under GNU GPL v. 3");
					goto StartProgram;

				case "start":
					CreateGame startGame = new CreateGame();
					startGame.startGame();
					break;

					case "quit":
						Environment.Exit(0);
						break;

				default:
					Console.WriteLine("That isn't a proper command!");
					goto StartProgram;
			}
		}
	}
}
