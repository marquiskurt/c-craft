# {C#} Craft

## About
C# Craft is a template project created in C# to show the fundamentals of the C# programming language. If desired, this can also turn into a videogame inside of a Terminal!

## Requirements:
* Visual Studio 2017 (Mac/Win)
* SourceTree (makes repository cloning easier)